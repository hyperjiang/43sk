define([
    'bootstrap-datepicker',
], function () {
    $('.date-picker').datepicker({
        language: GLOBAL.lang
    });
});
