require.config({
    map: {
        '*': {
            'css': 'lib/require-css/css'
        }
    },
    paths: {
        'jquery': 'lib/jquery-1.11.3',
        'bootstrap': 'lib/bootstrap-3.3.5',
        'bootstrap-datepicker': 'lib/bootstrap-datepicker/js/bootstrap-datepicker',
        'simplemde': 'lib/simplemde/simplemde.min'
    },
    shim: {
        'bootstrap': ['jquery'],
        'bootstrap-datepicker': [
            'bootstrap',
            'css!lib/bootstrap-datepicker/css/datepicker'
        ],
        'simplemde': [
            'css!lib/simplemde/simplemde.min'
        ]
    },
    packages: [
        {
            name: 'echarts',
            location: 'lib/echarts-2.2.7',
            main: 'echarts'
        },
        {
            name: 'zrender',
            location: 'lib/zrender-2.1.1',
            main: 'zrender'
        }
    ]
});
