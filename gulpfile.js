process.env.DISABLE_NOTIFIER = true;

var gulp = require('gulp');
var elixir = require('laravel-elixir');
var del = require('del');
var shell = require('gulp-shell');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {

    del(['public/css/*.map'], function (err) {});

    mix.styles([
        'bootstrap.css',
        'bootstrap-theme.css',
        'font-awesome.css',
        'app.css'
    ], 'public/css/global.css');

    mix.task('rjs');

});

gulp.task('rjs', function () {
    gulp.src('').pipe(shell('r.js -o app.build.js'));
});
