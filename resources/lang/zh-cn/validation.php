<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute 必须被接受',
    'active_url'           => ':attribute 不是一个有效的URL',
    'after'                => ':attribute 必须是一个在 :date 之后的日期',
    'alpha'                => ':attribute 只允许填写字母',
    'alpha_dash'           => ':attribute 只允许填写字母、数字和破折号',
    'alpha_num'            => ':attribute 只允许填写字母和数字',
    'array'                => ':attribute 必须是一个数组',
    'before'               => ':attribute 必须是一个在 :date 之前的日期',
    'between'              => [
        'numeric' => ':attribute 必须介于 :min 和 :max 之间',
        'file'    => ':attribute 必须介于 :min 和 :max kb 之间',
        'string'  => ':attribute 必须介于 :min 和 :max 个字符之间',
        'array'   => ':attribute 必须介于 :min 和 :max 个',
    ],
    'boolean'              => ':attribute 必须为布尔类型',
    'confirmed'            => ':attribute 与确认值不匹配',
    'date'                 => ':attribute 不是一个有效日期',
    'date_format'          => ':attribute 不符合格式 :format.',
    'different'            => ':attribute 必须不同于 :other ',
    'digits'               => ':attribute 必须为 :digits 个数字',
    'digits_between'       => ':attribute 必须介于 :min 和 :max 个数字',
    'email'                => ':attribute 必须是一个有效的邮箱地址',
    'exists'               => '被选中的 :attribute 是无效值',
    'filled'               => ':attribute 必须填写',
    'image'                => ':attribute 必须是图片',
    'in'                   => '被选中的 :attribute 是无效值',
    'integer'              => ':attribute 必须是整数',
    'ip'                   => ':attribute 必须是一个有效的IP地址',
    'json'                 => ':attribute 必须是一个有效的JSON字符串',
    'max'                  => [
        'numeric' => ':attribute 不能大于 :max.',
        'file'    => ':attribute 不能大于 :max kb',
        'string'  => ':attribute 不能多于 :max 个字符',
        'array'   => ':attribute 不能多于 :max 个',
    ],
    'mimes'                => ':attribute 必须为 :values 格式的文件',
    'min'                  => [
        'numeric' => ':attribute 必须不小于 :min.',
        'file'    => ':attribute 必须不小于 :min kb',
        'string'  => ':attribute 必须不少于 :min 个字符',
        'array'   => ':attribute 必须不少于 :min 个',
    ],
    'not_in'               => '被选中的 :attribute 是无效值',
    'numeric'              => ':attribute 必须为数字',
    'regex'                => ':attribute 格式不正确',
    'required'             => ':attribute 不能为空',
    'required_if'          => '当 :other 为 :value 时，:attribute field 不能为空',
    'required_unless'      => ':attribute 必须填写除非 :other 的值是 :values',
    'required_with'        => '当 :values 存在时，:attribute field 不能为空',
    'required_with_all'    => '当 :values 存在时，:attribute field 不能为空',
    'required_without'     => '当 :values 不存在时，:attribute field 不能为空',
    'required_without_all' => '当 :values 当中没有一个存在时，:attribute field 不能为空',
    'same'                 => ':attribute 和 :other 必须一致',
    'size'                 => [
        'numeric' => ':attribute 必须是 :size.',
        'file'    => ':attribute 必须是 :size kb',
        'string'  => ':attribute 必须包含 :size 个字符',
        'array'   => ':attribute 必须包含 :size 个条目',
    ],
    'string'               => ':attribute 必须是字符串',
    'timezone'             => ':attribute 必须是一个有效时区',
    'unique'               => ':attribute 已经被占用',
    'url'                  => ':attribute 格式错误',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'email'    => '邮箱',
        'password' => '密码',
    ],

];
