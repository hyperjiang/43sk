<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密码要求至少6位字符并且跟确认密码一致',
    'reset' => '您的密码已重置',
    'sent' => '我们已经把密码重置邮件发到了您的邮箱',
    'token' => '密码重置口令非法',
    'user' => "我们没找到该邮箱所属的用户",

];
