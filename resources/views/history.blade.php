@extends('layouts.default')

@section('content')
    <h3>{{ $pageTitle }}</h3>
    <table class="table table-striped small">
        <tr>
        <th>股票代码</th>
        <th>股票名称</th>
        <th>开盘</th>
        <th>收盘</th>
        <th>最低</th>
        <th>最高</th>
        <th>下影</th>
        <th>上影</th>
        <th>涨跌幅</th>
        <th>市盈率</th>
        <th>成交量(手)</th>
        <th>成交额(万)</th>
        <th>流通市值(亿)</th>
        <th>总市值(亿)</th>
        <th>日期</th>
        </tr>
        @foreach ($data as $row)
        <tr>
        <td><a href="{{ url("stock/{$stock->code}") }}" title="个股统计" target="_blank">{{ $row->code }}</a></td>
        <td><a href="{{ url("stock/{$stock->code}") }}" title="个股统计" target="_blank">{{ $row->name }}</a></td>
        <td>{{ $row->open }}</td>
        <td>{{ $row->close }}</td>
        <td>{{ $row->low }}</td>
        <td>{{ $row->high }}</td>
        <td><span class="c2">@if ($row->open > 0) {{ round(($row->low  - $row->open ) * 100 / $row->open , 2) }}% @else -- @endif</span></td>
        <td><span class="c1">@if ($row->open > 0) {{ round(($row->high  - $row->open ) * 100 / $row->open , 2) }}% @else -- @endif</span></td>
        <td>{!! color($row->rate, '%') !!}</td>
        <td>{{ $row->pe }}</td>
        <td>{{ round($row->volume / 100) }}</td>
        <td>{{ round($row->amount / 10000) }}</td>
        <td>{{ $row->market_value }}</td>
        <td>{{ $row->total_value }}</td>
        <td>{{ $row->date }}</td>
        </tr>
        @endforeach
    </table>

    {!! $data->render() !!}
@endsection
