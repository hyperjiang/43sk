@extends('layouts.default')

@section('css')
<style type="text/css">
.bgred {background:#FFE4E1;}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-10">
        <h3>{{ $stock->name }} ({{ $stock->code }}) 的统计情况</h3>

        <h4>
        @if ($stock->close > 0)
        价格：{{ $stock->close }}，涨幅：{!! color($stock->rate, '%') !!}
        @else
        停牌
        @endif
        ，换手：{{ $stock['ex'] }}%。

        @if ($stock->stat->trend > 0)
        连升了 {!! color($stock->stat->trend) !!} 个交易日。
        @else
        连跌了 {!! color($stock->stat->trend) !!} 个交易日。
        @endif

        @if ($stock->stat->line > 0)
        K线 {!! color($stock->stat->line) !!} 连阳。
        @else
        K线 {!! color($stock->stat->line) !!} 连阴。
        @endif
        </h4>

        <h4>平均上影：{{ $avgUpper }}%，平均下影：{{ $avgLower }}%，上影几率：{{ $upperRate }}%，下影几率：{{ $lowerRate }}%</h4>
        <h4>上涨概率：{{ $riseRate }}%，平均涨幅：{{ $avgRise }}%，下跌概率：{{ $fallRate }}%，平均跌幅：{{ $avgFall }}%</h4>
        <h4>低开概率：{{ $dRate }}%，高开概率：{{ $gRate }}%，平开概率：{{ $pRate }}%</h4>

        <div style="margin:0 15px;">
        <select id="techType">
        <option value="1">分时图</option>
        <option value="2">半年K线</option>
        <option value="3">1年K线</option>
        <option value="4">周K线</option>
        </select>

        <a href="{{ url("history/{$stock->code}") }}" title="历史价格" target="_blank">查看历史价格</a>
        </div>

        <div><img id="quote" src="http://image2.sinajs.cn/newchart/min/n/{{ $stock->area . $stock->code }}.gif" /></div>

        <div class="row">
            <div class="col-xs-6">
            <table class="table table-condensed small">
                <tr><th>上涨/跌日数</th><th>次数</th><th>上涨概率</th></tr>
                @foreach ($trend as $row)
                <tr {!! ($stock->stat->trend == $row->days) ? 'class="bgred"' : '' !!}><td>{{ $row->days }}</td><td>{{ $row->num }}</td><td>{{ $row->rate }}%</td></tr>
                @endforeach
            </table>
            </div>
            <div class="col-xs-6">
            <table class="table table-condensed small">
                <tr><th>连阳/阴日数</th><th>次数</th><th>收阳概率</th></tr>
                @foreach ($line as $row)
                <tr {!! ($stock->stat->line == $row->days) ? 'class="bgred"' : '' !!}><td>{{ $row->days }}</td><td>{{ $row->num }}</td><td>{{ $row->rate }}%</td></tr>
                @endforeach
            </table>
            </div>
        </div>
        <h3>所有股票统计参考</h3>
        <div class="row">
            <div class="col-xs-6">
            <table class="table table-condensed small">
                <tr align="left"><th>上涨/跌日数</th><th>次数</th><th>上涨概率</th></tr>
                @foreach ($allTrend as $row)
                <tr {!! ($stock->stat->trend == $row->days) ? 'class="bgred"' : '' !!}><td>{{ $row->days }}</td><td>{{ $row->num }}</td><td>{{ $row->rate }}%</td></tr>
                @endforeach
            </table>
            </div>
            <div class="col-xs-6">
            <table class="table table-condensed small">
                <tr align="left"><th>连阳/阴日数</th><th>次数</th><th>收阳概率</th></tr>
                @foreach ($allLine as $row)
                <tr {!! ($stock->stat->trend == $row->days) ? 'class="bgred"' : '' !!}><td>{{ $row->days }}</td><td>{{ $row->num }}</td><td>{{ $row->rate }}%</td></tr>
                @endforeach
            </table>
            </div>
        </div>
    </div>

    <div class="col-md-2" style="float:right">
        <script type="text/javascript"><!--
        google_ad_client = "ca-pub-4779592234647501";
        /* 160x600, 创建于 10-7-23 */
        google_ad_slot = "2643508217";
        google_ad_width = 160;
        google_ad_height = 600;
        //-->
        </script>
        <script type="text/javascript"
        src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
    </div>
</div>

<h3>涨跌幅股票数量统计</h3>
<table class="table table-bordered table-condensed small">
<tr><td><b>涨跌幅</b></td>
<? $d = $z = 0 ?>
<? foreach ($stat as $key => $val) { ?>
<? if ($key < 0) $d += $val; elseif ($key > 0) $z += $val; ?>
<td><?=$key?></td>
<? } ?>
</tr>
<tr><td><b>数量</b></td>
<? foreach ($stat as $key => $val) { ?>
<td <?=($val == max($stat) ? 'class="bgred"' : '')?>><?=$val?></td>
<? } ?>
</tr>
<tr><td><b>比例</b></td>
<? foreach ($stat as $key => $val) { ?>
<td <?=($val == max($stat) ? 'class="bgred"' : '')?>><?=round($val * 100 / $total, 2)?>%</td>
<? } ?>
</tr>
</table>

<div>下跌：<?=$d?>(<?=round($d * 100 / $total, 2)?>%)，横盘：<?=$stat[0]?>(<?=round($stat[0] * 100 / $total, 2)?>%)，上涨：<?=$z?>(<?=round($z * 100 / $total, 2)?>%)</div>
@endsection

@section('js')
<script type="text/javascript">
require(['jquery'], function ($) {
    $('#techType').change(function() {
        var key = Number($(this).val());
        var k = Math.random();
        var code = '{{ $stock->area . $stock->code }}';
        //分时图
        if (key == 1) {
            url = "http://image2.sinajs.cn/newchart/min/n/" + code + ".gif?";
            $("#quote").attr("src", url + k).attr("title", "分时图");
        }
        //半年K线
        if (key == 2) {
            url = "http://image2.sinajs.cn/newchart/daily_2/n/" + code + ".gif?";
            $("#quote").attr("src", url + k).attr("title", "半年K线");
        }
        //一年K线
        if (key == 3) {
            url = "http://image2.sinajs.cn/newchart/daily_3/n/" + code + ".gif?";
            $("#quote").attr("src", url + k).attr("title", "一年K线");
        }
        //周K线
        if (key == 4) {
            url = "http://image2.sinajs.cn/newchart/weekly/n/" + code + ".gif?";
            $("#quote").attr("src", url + k).attr("title", "周K线");
        }
    });
});
</script>
@endsection
