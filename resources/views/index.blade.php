@extends('layouts.default')

@section('content')

<form action="{{ url('index') }}" class="form-inline">

    <div class="form-group">
        <label>选择时间区间</label>
        <div class="input-group">
            <input type="text" id="start" name="start" class="form-control date-picker" value="{{ $start }}" class="text"/>
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        </div>
        ～
        <div class="input-group">
            <input type="text" id="end" name="end" class="form-control date-picker" value="{{ $end }}" class="text"/>
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        </div>
    </div>

    <input type="submit" value=" 确 定 " class="btn btn-primary" />

</form>

<table class="table table-bordered table-condensed small">
<tr><th>&nbsp;</th><th>目前</th><th>最高</th><th>最低</th></tr>
<tr>
<td><b>价格指数</b></td>
<td>{{ number_format($current['price']) }} ({{ $current['date'] }})</td>
<td>{{ number_format($maxPrice['price']) }} ({{ $maxPrice['date'] }}, <font color="Green">{{ round(($current['price'] - $maxPrice['price']) * 100 / $maxPrice['price'], 2) }}%</font>)</td>
<td>{{ number_format($minPrice['price']) }} ({{ $minPrice['date'] }}, <font color="Red">+{{ round(($current['price'] - $minPrice['price']) * 100 / $minPrice['price'], 2) }}%</font>)</td>
</tr>
<tr>
<td><b>成交量</b></td>
<td>{{ number_format($current['volume']) }} ({{ $current['date'] }})</td>
<td>{{ number_format($maxVolume['volume']) }} ({{ $maxVolume['date'] }}, <font color="Green">{{ round(($current['volume'] - $maxVolume['volume']) * 100 / $maxVolume['volume'], 2) }}%</font>)</td>
<td>{{ number_format($minVolume['volume']) }} ({{ $minVolume['date'] }}, <font color="Red">+{{ round(($current['volume'] - $minVolume['volume']) * 100 / $minVolume['volume'], 2) }}%</font>)</td>
</tr>
</table>

<div id="chart" style="height:500px"></div>

<div id="chart2" style="height:500px"></div>

@endsection

@section('js')
<script type="text/javascript">
require(
    [
        'echarts',
        'echarts/chart/line',
        'echarts/chart/bar'
    ],
    function (ec) {
        $.ajax({
            url: "{{ url('chart-index') }}",
            data: {start: '{{ $start}}', end: '{{ $end }}'},
            dataType: "jsonp",
            jsonp: "callback"
        }).done(function(option) {
            var chart = ec.init(document.getElementById('chart'));
            chart.setOption(option);
        });

        $.ajax({
            url: "{{ url('chart-adi') }}",
            data: {start: '{{ $start}}', end: '{{ $end }}'},
            dataType: "jsonp",
            jsonp: "callback"
        }).done(function(option) {
            var chart = ec.init(document.getElementById('chart2'));
            chart.setOption(option);
        });
    }
);
</script>
@endsection
