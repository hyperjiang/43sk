@extends('layouts.default')

@section('content')

@can('upload-stock')
    <form name="stock" method="post" enctype="multipart/form-data">
    {!! csrf_field() !!}

    <div class="form-group">
        <input type="file" name="ag">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">上传</button>
    </div>
</form>
@else
    <div class="alert alert-danger">你没有权限</div>
@endcan

@endsection
