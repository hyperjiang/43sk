@extends('layouts.default')

@section('content')
    <form action="{{ url('month-stat') }}" class="form-inline">
        <div class="form-group">
            <label>指数类型</label>
            <select name="type" class="form-control">
                <option value="sh" {{ select('sh', $type) }}>上证综指</option>
                <option value="sz" {{ select('sz', $type) }}>深证成指</option>
                <option value="hs" {{ select('hs', $type) }}>沪深300</option>
                <option value="zx" {{ select('zx', $type) }}>中小板指</option>
                <option value="cy" {{ select('cy', $type) }}>创业板指</option>
            </select>
        </div>
    </form>

    <table class="table table-striped small">
    <tr><th>年份\月份</th><th>一月</th><th>二月</th><th>三月</th><th>四月</th><th>五月</th><th>六月</th><th>七月</th><th>八月</th><th>九月</th><th>十月</th><th>十一月</th><th>十二月</th></tr>
    @foreach ($data as $year => $row)
    <tr>
    <td>{{ $year }}</td>
    @for ($i = 1; $i <= 12; $i++)
    <td @if (isset($row[$i])) style="color:{{ $row[$i] > 0 ? 'red' : 'green' }}; {{ abs($row[$i]) >= 10 ? 'font-weight:bold; font-size:16px;' : '' }} @endif">{{ $row[$i] or '--' }}</td>
    @endfor
    </tr>
    @endforeach
    <tr>
    <th>累计涨跌幅</th>
    @for ($i = 1; $i <= 12; $i++)
    <th>{!! color(round($stat[$i], 2)) !!}</th>
    @endfor
    </tr>
    <tr>
    <th>上涨概率</th>
    @for ($i = 1; $i <= 12; $i++)
    <th style="color:{{ $ratio[$i] > 50 ? 'red' : 'green' }}; {{ $ratio[$i] >= 70 ? 'font-size:16px;' : '' }}">{{ $ratio[$i] }}%</th>
    @endfor
    </tr>
    </table>
@endsection

@section('js')
<script type="text/javascript">
require(['jquery'], function ($) {
    $(document).ready(function() {
        $('select').change(function() {
            $("form:first").submit();
        });
    });
});
</script>
@endsection
