@extends('layouts.default')

@section('content')
    <form action="{{ url() }}" class="form-inline">
    <input type="hidden" id="sort" name="sort" value="{{ $sort }}" />
    <input type="hidden" name="own" value="{{ $own }}" />

    <div class="form-group">
        <label>股票代码/名称</label>
        <input type="text" class="form-control" name="keyword" value="{{ $keyword }}">
    </div>
    <div class="form-group">
        <label>上涨概率大于</label>
        <input type="text" class="form-control" name="chance" value="{{ $chance }}" size=4>
    </div>
    <div class="form-group">
        <select class="form-control" name="direct"><option value="desc">降序</option><option value="asc" {{ select($direct, 'asc') }}>升序</option></select>
    </div>
    <input type="submit" value=" 搜 索 " class="btn btn-primary" />

    </form>

    <table class="table table-striped small">
        <tr><th id="code" class="sort">股票代码{!! arrow('code', $sort, $direct) !!}</th>
        <th id="name" class="sort">股票名称{!! arrow('name', $sort, $direct) !!}</th>
        <th id="open" class="sort">开盘{!! arrow('open', $sort, $direct) !!}</th>
        <th id="close" class="sort">收盘{!! arrow('close', $sort, $direct) !!}</th>
        <th id="last" class="sort">昨收{!! arrow('last', $sort, $direct) !!}</th>
        <th id="low" class="sort">最低{!! arrow('low', $sort, $direct) !!}</th>
        <th id="high" class="sort">最高{!! arrow('high', $sort, $direct) !!}</th>
        <th id="rate" class="sort">涨跌幅{!! arrow('rate', $sort, $direct) !!}</th>
        <th id="pe" class="sort">市盈{!! arrow('pe', $sort, $direct) !!}</th>
        <th id="ex" class="sort">换手{!! arrow('ex', $sort, $direct) !!}</th>
        <th id="volume" class="sort">成交量(手){!! arrow('volume', $sort, $direct) !!}</th>
        <th id="amount" class="sort">成交额(万){!! arrow('amount', $sort, $direct) !!}</th>
        <th id="trend" class="sort">连涨/跌{!! arrow('trend', $sort, $direct) !!}</th>
        <th id="line" class="sort">连阳/阴{!! arrow('line', $sort, $direct) !!}</th>
        <th id="market_value" class="sort">流通市值(亿){!! arrow('market_value', $sort, $direct) !!}</th>
        <th id="total_value" class="sort">总市值(亿){!! arrow('total_value', $sort, $direct) !!}</th>
        @if ($own and Auth::check())
        <th id="add_value" class="sort">加入市值(亿){!! arrow('add_value', $sort, $direct) !!}</th>
        <th id="date" class="sort">加入日期{!! arrow('date', $sort, $direct) !!}</th>
        <th id="addup" class="sort">累计涨跌{!! arrow('addup', $sort, $direct) !!}</th>
        @endif
        <th>操作</th>
        </tr>
        @foreach ($stocks as $stock)
        <tr>
        <td><a href="{{ url("stock/{$stock->code}") }}" title="个股统计" target="_blank">{!! light_keyword($stock->code, $keyword) !!}</a></td>
        <td><a href="{{ url("stock/{$stock->code}") }}" title="个股统计" target="_blank">{!! light_keyword($stock->name, $keyword) !!}</a></td>
        <td>{{ $stock->open }}</td><td>{{ $stock->close }}</td><td>{{ $stock->last }}</td>
        <td>{{ $stock->low }}</td><td>{{ $stock->high }}</td><td>{!! color($stock->rate, '%') !!}</td>
        <td>{{ $stock->pe }}</td><td>{{ $stock->ex }}</td>
        <td>{{ round($stock->volume  / 100) }}</td><td>{{ round($stock->amount  / 10000) }}</td>
        <td>{!! color($stock->trend) !!}</td>
        <td>{!! color($stock->line) !!}</td>
        <td>{{ $stock->market_value }}</td>
        <td>{{ $stock->total_value }}</td>
        @if ($own and Auth::check())
        <td>{{ $stock->add_value }}</td>
        <td>{{ $stock->date }}</td>
        <td>{!! color($stock->addup, '%') !!}</td>
        @endif
        <td>
        @if ($own)
        <a href="{{ url("stock/delete/{$stock->code}") }}" title="删除自选股"><i class="fa fa-minus-circle"></i></a>
        @else
        <a href="{{ url("stock/add/{$stock->code}") }}" title="加入自选股"><i class="fa fa-plus-circle"></i></a>
        @endif
        <a href="{{ url("history/{$stock->code}") }}" title="历史价格" target="_blank"><i class="fa fa-bar-chart"></i></a>
        <a href="{{ url("pe/{$stock->code}") }}" title="市盈率变化" target="_blank"><i class="fa fa-line-chart"></i></a>
        </td>
        </tr>
        @endforeach
    </table>

    {!! $stocks->render() !!}
@endsection

@section('js')
<script type="text/javascript">
require(['jquery'], function ($) {
    $(document).ready(function() {
        var form = $("form:first");
        $('select').change(function() {
            form.submit();
        });
        $('th.sort').click(function() {
            $('#sort').val($(this).attr('id'));
            form.submit();
        });
    });
});
</script>
@endsection
