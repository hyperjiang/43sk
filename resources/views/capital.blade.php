@extends('layouts.default')

@section('content')

<form action="{{ url('capital') }}" class="form-inline">

    <div class="form-group">
        <label>选择时间区间</label>
        <div class="input-group">
            <input type="text" id="start" name="start" class="form-control date-picker" value="{{ $start }}" class="text"/>
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        </div>
        ～
        <div class="input-group">
            <input type="text" id="end" name="end" class="form-control date-picker" value="{{ $end }}" class="text"/>
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        </div>
    </div>

    <input type="submit" value=" 确 定 " class="btn btn-primary" />

</form>

<table class="table table-bordered table-condensed small">
<tr><th>&nbsp;</th><th>上海</th><th>深圳</th><th>两市</th></tr>

@foreach ($columns as $column => $desc)
<tr class="{{ $colors[$column] }}">
<td><b>最新{{ $desc }}</b></td>
<td>{{ $current['sh'][$column] }} ({{ $current['sh']['date'] }})</td>
<td>{{ $current['sz'][$column] }} ({{ $current['sz']['date'] }})</td>
<td>{{ $current['all'][$column] }} ({{ $current['all']['date'] }})</td>
</tr>
<tr class="{{ $colors[$column] }}">
<td><b>最低{{ $desc }}</b></td>
<td>{{ $minData['sh'][$column]['value'] }} ({{ $minData['sh'][$column]['date'] }})</td>
<td>{{ $minData['sz'][$column]['value'] }} ({{ $minData['sz'][$column]['date'] }})</td>
<td>{{ $minData['all'][$column]['value'] }} ({{ $minData['all'][$column]['date'] }})</td>
</tr>
<tr class="{{ $colors[$column] }}">
<td><b>最高{{ $desc }}</b></td>
<td>{{ $maxData['sh'][$column]['value'] }} ({{ $maxData['sh'][$column]['date'] }})</td>
<td>{{ $maxData['sz'][$column]['value'] }} ({{ $maxData['sz'][$column]['date'] }})</td>
<td>{{ $maxData['all'][$column]['value'] }} ({{ $maxData['all'][$column]['date'] }})</td>
</tr>
@endforeach

</table>

<div id="chart" style="height:500px"></div>

<div id="chart2" style="height:500px"></div>

<div id="chart3" style="height:500px"></div>

<div id="chart4" style="height:500px"></div>

@endsection

@section('js')
<script type="text/javascript">
require(
    [
        'echarts',
        'echarts/chart/line'
    ],
    function (ec) {
        $.ajax({
            url: "{{ url('chart-capital') }}",
            data: {start: '{{ $start}}', end: '{{ $end }}', area: 'sh'},
            dataType: "jsonp",
            jsonp: "callback"
        }).done(function(option) {
            var chart = ec.init(document.getElementById('chart'));
            chart.setOption(option);
        });

        $.ajax({
            url: "{{ url('chart-capital') }}",
            data: {start: '{{ $start}}', end: '{{ $end }}', area: 'sz'},
            dataType: "jsonp",
            jsonp: "callback"
        }).done(function(option) {
            var chart = ec.init(document.getElementById('chart2'));
            chart.setOption(option);
        });

        $.ajax({
            url: "{{ url('chart-turnover') }}",
            data: {start: '{{ $start}}', end: '{{ $end }}'},
            dataType: "jsonp",
            jsonp: "callback"
        }).done(function(option) {
            var chart = ec.init(document.getElementById('chart3'));
            chart.setOption(option);
        });

        $.ajax({
            url: "{{ url('chart-ex') }}",
            data: {start: '{{ $start}}', end: '{{ $end }}'},
            dataType: "jsonp",
            jsonp: "callback"
        }).done(function(option) {
            var chart = ec.init(document.getElementById('chart4'));
            chart.setOption(option);
        });
    }
);
</script>
@endsection
