<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">编辑用户信息</h4>
</div>
<div class="modal-body">
    <form method="POST" action="/user/info/{{ $user->id }}">
        {!! csrf_field() !!}

        <div class="form-group">
            <label>用户名</label>
            <input type="text" name="name" value="{{ $user->name }}" class="form-control">
        </div>

        <div class="form-group">
            <label>密码</label>
            <input type="password" name="password" class="form-control" placeholder="如无需修改请留空">
        </div>

        <div class="form-group">
            <label>确认密码</label>
            <input type="password" name="password_confirmation" class="form-control" placeholder="如无需修改请留空">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-warning">确定</button>
        </div>
    </form>
</div>
