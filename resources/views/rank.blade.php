@extends('layouts.default')

@section('content')
    <form action="{{ url('rank') }}" class="form-inline">
    <input type="hidden" id="sort" name="sort" value="{{ $sort }}" />

    <div class="form-group">
        <select class="form-control" name="type">
            <option value="week" {{ ($type == 'week') ? 'selected' : '' }}>本周涨跌榜</option>
            <option value="month" {{ ($type == 'month') ? 'selected' : '' }}>本月涨跌榜</option>
            <option value="year" {{ ($type == 'year') ? 'selected' : '' }}>本年涨跌榜</option>
        </select>
    </div>

    <div class="form-group">
        <select class="form-control" name="direct"><option value="desc">降序</option><option value="asc" {{ select($direct, 'asc') }}>升序</option></select>
    </div>

    <div class="form-group">
        <label>股票代码/名称：</label>
        <input type="text" class="form-control" name="keyword" value="{{ $keyword }}">
    </div>

    <input type="submit" value=" 搜 索 " class="btn btn-primary" />
    </form>

    <div class="alert alert-info" role="alert">注：价格未复权，所以除权的股票的涨跌幅是不准确的。</div>

    <table class="table table-striped small">
        <tr><th id="code" class="sort">股票代码{!! arrow('code', $sort, $direct) !!}</th>
        <th id="name" class="sort">股票名称{!! arrow('name', $sort, $direct) !!}</th>
        <th id="open" class="sort">开盘{!! arrow('open', $sort, $direct) !!}</th>
        <th id="close" class="sort">收盘{!! arrow('close', $sort, $direct) !!}</th>
        <th id="rate" class="sort">涨跌幅{!! arrow('rate', $sort, $direct) !!}</th>
        <th id="pe" class="sort">市盈{!! arrow('pe', $sort, $direct) !!}</th>
        <th id="trend" class="sort">连涨/跌{!! arrow('trend', $sort, $direct) !!}</th>
        <th id="line" class="sort">连阳/阴{!! arrow('line', $sort, $direct) !!}</th>
        <th>操作</th>
        </tr>
        @foreach ($data as $stock)
        <tr>
        <td><a href="{{ url("stock/{$stock->code}") }}" title="个股统计" target="_blank">{!! light_keyword($stock->code, $keyword) !!}</a></td>
        <td><a href="{{ url("stock/{$stock->code}") }}" title="个股统计" target="_blank">{!! light_keyword($stock->name, $keyword) !!}</a></td>
        <td>{{ $stock->open }}</td>
        <td>{{ $stock->close }}</td>
        <td>{!! color($stock->rate, '%') !!}</td>
        <td>{{ $stock->pe }}</td>
        <td>{!! color($stock->trend) !!}</td>
        <td>{!! color($stock->line) !!}</td>
        <td>
        <a href="{{ url("history/{$stock->code}") }}" title="历史价格" target="_blank"><i class="fa fa-bar-chart"></i></a>
        <a href="{{ url("pe/{$stock->code}") }}" title="市盈率变化" target="_blank"><i class="fa fa-line-chart"></i></a>
        </td>
        </tr>
        @endforeach
    </table>

    {!! $data->render() !!}
@endsection

@section('js')
<script type="text/javascript">
require(['jquery'], function ($) {
    $(document).ready(function() {
        var form = $("form:first");
        $('select').change(function() {
            form.submit();
        });
        $('th.sort').click(function() {
            $('#sort').val($(this).attr('id'));
            form.submit();
        });
    });
});
</script>
@endsection
