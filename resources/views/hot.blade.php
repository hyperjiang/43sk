@extends('layouts.default')

@section('content')

    <form action="{{ url('hot') }}" class="form-inline">
        <div class="form-group">
            <label>选择时间区间</label>
            <div class="input-group">
                <input type="text" id="start" name="start" class="form-control date-picker" value="{{ $start }}" class="text"/>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
            ～
            <div class="input-group">
                <input type="text" id="end" name="end" class="form-control date-picker" value="{{ $end }}" class="text"/>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>

        <div class="form-group">
            <label>总换手</label>
            <div class="input-group">
                <input type="text" name="rate" value="<?=$rate?>" size="4" class="form-control" />
                <span class="input-group-addon">%</span>
            </div>
        </div>

        <input type="submit" value=" 确 定 " class="btn btn-primary" />
    </form>

    <table class="table table-striped small">
        <tr>
        <th>股票代码</th>
        <th>股票名称</th>
        <th>换手</th>
        <th>均价</th>
        <th>开盘</th>
        <th>收盘</th>
        <th>涨跌幅</th>
        <th>成交量(手)</th>
        <th>成交额(万)</th>
        <th>操作</th>
        </tr>
        @foreach ($data as $row)
        <tr>
        <td><a href="{{ url("stock/{$row->code}") }}" title="个股统计" target="_blank">{{ $row->code }}</a></td>
        <td><a href="{{ url("stock/{$row->code}") }}" title="个股统计" target="_blank">{{ $row->name }}</a></td>
        <td>{{ $row->ex }}</td>
        <td>{{ round($row->avg, 2) }}</td>
        <td>{{ $row->open }}</td>
        <td>{{ $row->close }}</td>
        <td>{!! color($row->rate, '%') !!}</td>
        <td>{{ round($row->volume / 100) }}</td>
        <td>{{ round($row->amount / 10000) }}</td>
        <td>
        <a href="{{ url("history/{$row->code}") }}" title="历史价格" target="_blank"><i class="fa fa-bar-chart"></i></a>
        <a href="{{ url("pe/{$row->code}") }}" title="市盈率变化" target="_blank"><i class="fa fa-line-chart"></i></a>
        </td>
        </tr>
        @endforeach
    </table>

@endsection
