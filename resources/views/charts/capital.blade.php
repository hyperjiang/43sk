{{ $callback }}({
    title : {
        text: '{{ $title }}',
        subtext: '{{ $start }} ～ {{ $end }}',
        x: 'center'
    },
    legend: {
        data: ['总市值', '流通市值', '市盈率'],
        x: 'left'
    },
    tooltip: {
        trigger: 'axis',
        formatter: function (params) {
            var res = params[0].name;
            for (var i = 0, l = params.length; i < l; i++) {
                res += '<br/>' + params[i].seriesName + ' : ' + params[i].value + params[i].data.extra;
            }
            return res;
        }
    },
    toolbox: {
        show: true,
        feature: {
            mark: {show: false},
            dataView: {show: true, readOnly: false},
            magicType: {show: false},
            restore: {show: false},
            saveAsImage: {show: true}
        }
    },
    xAxis: [
        {
            type: 'category',
            data: {!! $label !!}
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: '市值（亿元）',
            scale: true
        },
        {
            type: 'value',
            name: '市盈率',
            scale: true
        }
    ],
    series: [
        {
            name: '总市值',
            type: 'line',
            data: {!! $total !!}
        },
        {
            name: '流通市值',
            type: 'line',
            data: {!! $market !!}
        },
        {
            name: '市盈率',
            type: 'line',
            yAxisIndex: 1,
            data: {!! $pe !!}
        }
    ]
});
