{{ $callback }}({
    title : {
        text: '死多指数',
        subtext: '{{ $start }} ～ {{ $end }}',
        x: 'center'
    },
    tooltip: {
        trigger: 'axis',
        formatter: function (params) {
            var res = params[0].name;
            for (var i = 0, l = params.length; i < l; i++) {
                res += '<br/>' + params[i].seriesName + ' : ' + params[i].value + params[i].data.extra;
            }
            return res;
        }
    },
    toolbox: {
        show: true,
        feature: {
            mark: {show: false},
            dataView: {show: true, readOnly: false},
            magicType: {show: false},
            restore: {show: false},
            saveAsImage: {show: true}
        }
    },
    xAxis: [
        {
            type: 'category',
            data: {!! $label !!}
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: '价格指数',
            scale: true
        },
        {
            type: 'value',
            name: '成交量指数'
        }
    ],
    series: [
        {
            name: '价格指数',
            type: 'line',
            data: {!! $price !!},
            z: 2,
            itemStyle: {
                normal: {
                    color: '#0F67A1'
                }
            }
        },
        {
            name: '成交量指数',
            type: 'bar',
            yAxisIndex: 1,
            data: {!! $volume !!},
            z: 1,
            itemStyle: {
                normal: {
                    color: function(params) {
                        if (params.dataIndex == '-1') return '#FE8463';
                        return (params.data.extra) ? '#9BCA63' : '#FE8463';
                    }
                }
            }
        }
    ]
});
