{{ $callback }}({
    title : {
        text: '{{ $title }}',
        subtext: '{{ $start }} ～ {{ $end }}',
        x: 'center'
    },
    tooltip: {
        trigger: 'axis'
    },
    toolbox: {
        show: true,
        feature: {
            mark: {show: false},
            dataView: {show: true, readOnly: false},
            magicType: {show: false},
            restore: {show: false},
            saveAsImage: {show: true}
        }
    },
    legend: {
        data: ['市盈率', '股价'],
        x: 'left'
    },
    xAxis: [
        {
            type: 'category',
            data: {!! $label !!}
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: '市盈率',
            scale: true
        },
        {
            type: 'value',
            name: '股价',
            scale: true
        }
    ],
    series: [
        {
            name: '市盈率',
            type: 'line',
            data: {!! $pe !!}
        },
        {
            name: '股价',
            type: 'line',
            yAxisIndex: 1,
            data: {!! $price !!}
        }
    ]
});
