{{ $callback }}({
    title : {
        text: '{{ $title }}',
        subtext: '{{ $start }} ～ {{ $end }}',
        x: 'center'
    },
    legend: {
        data: ['总换手率', '上海', '深圳'],
        x: 'left'
    },
    tooltip: {
        trigger: 'axis',
        formatter: '{b} <br/>{a0} : {c0}%<br/>{a1} : {c1}%<br/>{a2} : {c2}%'
    },
    toolbox: {
        show: true,
        feature: {
            mark: {show: false},
            dataView: {show: true, readOnly: false},
            magicType: {show: false},
            restore: {show: false},
            saveAsImage: {show: true}
        }
    },
    xAxis: [
        {
            type: 'category',
            data: {!! $label !!}
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: '换手率',
            scale: true
        }
    ],
    series: [
        {
            name: '总换手率',
            type: 'line',
            data: {!! $total !!}
        },
        {
            name: '上海',
            type: 'line',
            data: {!! $sh !!}
        },
        {
            name: '深圳',
            type: 'line',
            data: {!! $sz !!}
        }
    ]
});
