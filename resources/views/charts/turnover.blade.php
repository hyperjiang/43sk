{{ $callback }}({
    title : {
        text: '{{ $title }}',
        subtext: '{{ $start }} ～ {{ $end }}',
        x: 'center'
    },
    legend: {
        data: ['总成交额', '上海', '深圳'],
        x: 'left'
    },
    tooltip: {
        trigger: 'axis'
    },
    toolbox: {
        show: true,
        feature: {
            mark: {show: false},
            dataView: {show: true, readOnly: false},
            magicType: {show: false},
            restore: {show: false},
            saveAsImage: {show: true}
        }
    },
    xAxis: [
        {
            type: 'category',
            data: {!! $label !!}
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: '成交额（亿元）',
            scale: true
        }
    ],
    series: [
        {
            name: '总成交额',
            type: 'line',
            data: {!! $total !!}
        },
        {
            name: '上海',
            type: 'line',
            data: {!! $sh !!}
        },
        {
            name: '深圳',
            type: 'line',
            data: {!! $sz !!}
        }
    ]
});
