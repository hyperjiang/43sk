{{ $callback }}({
    title : {
        text: 'AD指数',
        subtext: '{{ $start }} ～ {{ $end }}',
        x: 'center'
    },
    tooltip: {
        trigger: 'axis'
    },
    toolbox: {
        show: true,
        feature: {
            mark: {show: false},
            dataView: {show: true, readOnly: false},
            magicType: {show: false},
            restore: {show: false},
            saveAsImage: {show: true}
        }
    },
    xAxis: [
        {
            type: 'category',
            data: {!! $label !!}
        }
    ],
    yAxis: [
        {
            type: 'value',
            scale: true
        }
    ],
    series: [
        {
            name: 'AD指数',
            type: 'bar',
            data: {!! $value !!},
            itemStyle: {
                normal: {
                    color: function(params) {
                        return params.data > 0 ? '#FE8463' : '#9BCA63';
                    }
                }
            }
        }
    ]
});
