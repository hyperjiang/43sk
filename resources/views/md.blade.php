@extends('layouts.default')

@section('content')

<textarea id="content" name="content" rows="3">abc</textarea>

@endsection

@section('js')
<script>
require(['jquery', 'simplemde'], function ($, SimpleMDE) {
    var simplemde = new SimpleMDE({
        toolbar: ["bold", "italic", "heading", "|",
        "quote", "unordered-list", "ordered-list", "|",
        "link", "image", "preview"],
    });
});
</script>
@endsection
