@extends('layouts.default')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">密码重置</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <form method="POST" action="/password/reset">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="form-group">
                    邮箱
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                </div>

                <div class="form-group">
                    密码
                    <input type="password" name="password" class="form-control">
                </div>

                <div class="form-group">
                    确认密码
                    <input type="password" name="password_confirmation" class="form-control">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-warning">
                        重置密码
                    </button>
                </div>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>

@endsection
