@extends('layouts.default')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">请登录或注册</h3>
    </div>
    <div class="panel-body">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="col-md-4">
            <form method="POST" action="/login">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label>邮箱</label>
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label>密码</label>
                    <input type="password" name="password" id="password" class="form-control">
                    <a href="/password/email" data-toggle="modal" data-target="#modal-500">忘记密码？</a>
                </div>

                <div class="form-group">
                    <input type="checkbox" name="remember"> 保存密码，下次自动登录
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-warning">登录</button>
                </div>
            </form>
        </div>

        <div class="col-md-2"></div>

        <div class="col-md-4" style="border-left: 1px dashed #d0d0d0;">
            <form method="POST" action="/register">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label>用户名</label>
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label>邮箱</label>
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label>密码</label>
                    <input type="password" name="password" class="form-control">
                </div>

                <div class="form-group">
                    <label>确认密码</label>
                    <input type="password" name="password_confirmation" class="form-control">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-warning">注册</button>
                </div>
            </form>
        </div>

        <div class="col-md-2"></div>
    </div>
</div>

@endsection
