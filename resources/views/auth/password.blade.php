<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">发送密码重置邮件</h4>
</div>
<div class="modal-body">
    <form method="POST" action="/password/email">
        {!! csrf_field() !!}

        <div class="form-group">
            <label>邮箱</label>
            <input type="email" name="email" value="{{ old('email') }}" class="form-control">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                发送
            </button>
        </div>
    </form>
</div>
