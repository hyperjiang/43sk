@extends('layouts.default')

@section('content')
<p>本网站是由个人维护的非营利性网站，网站程序均系自主开发，数据每个交易日收盘后更新，本站内容不代表本人观点，仅供参考。</p>
<p>本网站程序基于 Laravel 5.1 开发，源代码全部开源，托管地址：<a href="https://gitlab.com/hyperjiang/43sk" target="_blank">https://gitlab.com/hyperjiang/43sk</a></p>

@endsection
