@extends('layouts.default')

@section('content')

<form action="{{ url('pe/' . $stock->code) }}" class="form-inline">

    <div class="form-group">
        <label>选择时间区间</label>
        <div class="input-group">
            <input type="text" id="start" name="start" class="form-control date-picker" value="{{ $start }}" class="text"/>
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        </div>
        ～
        <div class="input-group">
            <input type="text" id="end" name="end" class="form-control date-picker" value="{{ $end }}" class="text"/>
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        </div>
    </div>

    <input type="submit" value=" 确 定 " class="btn btn-primary" />

</form>

<div id="chart" style="height:500px"></div>

@endsection

@section('js')
<script type="text/javascript">
require(
    [
        'echarts',
        'echarts/chart/line'
    ],
    function (ec) {
        $.ajax({
            url: "{{ url('chart-pe/' . $stock->code) }}",
            data: {start: '{{ $start}}', end: '{{ $end }}'},
            dataType: "jsonp",
            jsonp: "callback"
        }).done(function(option) {
            var chart = ec.init(document.getElementById('chart'));
            chart.setOption(option);
        });
    }
);
</script>
@endsection
