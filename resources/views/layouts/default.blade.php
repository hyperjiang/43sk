<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="{{ $metaKeywords or 'A股,股票,中国,股市,理财,财经,stock' }}" />
    <meta name="description" content="{{ $metaDesc or '本网站是由个人维护的非营利性网站，主要提供股票分析数据。' }}" />

    <title>{{ $pageTitle or '我们都是死多客 | 43sk.com' }}</title>

    <link rel="stylesheet" href="{{ asset('css/global.css') }}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('css')
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/">43sk.com</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="/">首页</a></li>
                <li class="{{ Request::is('rank') ? 'active' : '' }}"><a href="/rank">涨跌榜</a></li>
                <li class="{{ Request::is('hot') ? 'active' : '' }}"><a href="/hot">热股</a></li>
                <li class="{{ Request::is('index') ? 'active' : '' }}"><a href="/index">死多指数</a></li>
                <li class="{{ Request::is('capital') ? 'active' : '' }}"><a href="/capital">两市市值</a></li>
                <li class="{{ Request::is('month-stat') ? 'active' : '' }}"><a href="/month-stat">月份涨跌</a></li>
                <li class="{{ Request::is('about') ? 'active' : '' }}"><a href="/about">关于</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                @if (Auth::check())
                <li>
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <span class="username">{{ Auth::user()->name }}</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('stock.list', ['own' => 1]) }}">
                            <i class="fa fa-user"></i> 自选股
                        </a>
                    </li>
                    <li>
                        <a data-toggle="modal" data-target="#modal-500" href="{{ url('user/info/' . Auth::user()->id) }}">
                            <i class="fa fa-wrench"></i> 修改资料
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('logout') }}">
                            <i class="fa fa-key"></i> 退出登录
                        </a>
                    </li>
                </ul>
                </li>
                @else
                <li><a href="/login">登录</a></li>
                @endif
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </nav>

        <div class="container">
            @yield('content')
        </div>

        @include('parts.modals')

        <script type="text/javascript">
        window.GLOBAL = window.GLOBAL || {};
        GLOBAL.lang   = '{{ config('app.locale') }}';
        </script>

        {!! require_js() !!}

        <div style="margin:5px auto; width:728px;">
        <script type="text/javascript"><!--
        google_ad_client = "ca-pub-4779592234647501";
        /* 43sk_728_90 */
        google_ad_slot = "1489599022";
        google_ad_width = 728;
        google_ad_height = 90;
        //-->
        </script>
        <script type="text/javascript"
        src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
        </div>

        <!-- 底部开始 -->
        <div id="footer" align="center">
        Copyright &copy; 2010-{{ date('Y') }} 43sk.com All rights reserved.
        </div>
        <!-- 底部结束 -->

        @yield('js')

        <div style="display:none"><script src="http://s14.cnzz.com/stat.php?id=2514552&web_id=2514552" language="JavaScript"></script></div>
    </body>
</html>
