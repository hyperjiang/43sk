({
    "appDir": "public/src",
    "baseUrl": ".",
    "dir": "public/js/",
    "optimizeCss": "standard.keepLines",
    "mainConfigFile": "public/src/config.js",
    "skipDirOptimize": true,
    "normalizeDirDefines": "skip",
    "preserveLicenseComments": false,
    "findNestedDependencies": true,
    "stubModules": [],
    "modules": [
        {
            "name": "config",
            "include": [
                "jquery",
                "bootstrap",
                "bootstrap-datepicker",
                "simplemde"
            ]
        },
        {
            "name": "echarts",
            "exclude": ["config"]
        }
    ]
})
