<?php

namespace App\Http\Controllers;

use EasyWeChat\Foundation\Application;
use EasyWeChat\Message\Text;
use Illuminate\Http\Request;
use Log;

class WxController extends Controller
{
    public $app = null;

    public function __construct()
    {
        $options = [
            'debug'  => true,
            'app_id' => env('WX_ID'),
            'secret' => env('WX_SECRET'),
            'token'  => env('WX_TOKEN'),
            'oauth'  => [
                'scopes'   => ['snsapi_userinfo'],
                'callback' => '/wx/oauth_callback',
            ],
            'log'    => [
                'level' => 'debug',
                'file'  => storage_path('logs/easywechat.log'),
            ],
        ];

        $this->app = new Application($options);
    }

    public function handle(Request $request)
    {
        $server = $this->app->server;
        $user   = $this->app->user;
        $notice = $this->app->notice;

        $server->setMessageHandler(function ($message) use ($user, $notice) {
            Log::info(var_export($message, true));
            switch ($message->MsgType) {
                case 'event':

                    if ($message->Event == 'scancode_waitmsg' || $message->Event == 'scancode_push') {
                        $text = new Text(['content' => '你扫描的二维码链接是 ' . $message->ScanCodeInfo['ScanResult']]);
                        return $text;
                    } elseif ($message->Event == 'CLICK') {
                        $userId     = $message->FromUserName;
                        $fromUser   = $user->get($userId);
                        $templateId = 'p2y8OONToG_qNmiLFIQrXrK17L9bxyO0F2NIQXw6BK8';
                        $color      = '#FF0000';
                        $data       = [
                            'p' => $fromUser->nickname,
                        ];
                        $messageId = $notice->send([
                            'touser'      => $userId,
                            'template_id' => $templateId,
                            'url'         => $url,
                            'topcolor'    => $color,
                            'data'        => $data,
                        ]);
                        Log::info($messageId);
                    }
                    return;

                default:
                    $fromUser = $user->get($message->FromUserName);
                    return "{$fromUser->nickname} 您好!\n第二行\n<a href='http://43sk.com'>链接</a>";
            }

        });

        $server->serve()->send();
    }

    public function sendMsg(Request $request)
    {
        $userId     = $request->input('uid');
        // $fromUser   = $this->app->user->get($userId);
        $templateId = 'p2y8OONToG_qNmiLFIQrXrK17L9bxyO0F2NIQXw6BK8';
        $color      = '#FF0000';
        $data       = [
            'p' => "第一行\n第二行\n这是一个 <a href='http://43sk.com'>链接</a>",
        ];
        $messageId = $this->app->notice->send([
            'touser'      => $userId,
            'template_id' => $templateId,
            'topcolor'    => $color,
            'data'        => $data,
        ]);
    }

    public function getMenu()
    {
        $menu = $this->app->menu;
        print_r($menu->all());
        print_r($menu->current());
    }

    public function createMenu()
    {
        $buttons = [
            [
                "name"       => "扫码",
                "sub_button" => [
                    [
                        "type" => "scancode_waitmsg",
                        "name" => "扫码带提示",
                        "key"  => "rselfmenu_0_0",
                    ],
                    [
                        "type" => "scancode_push",
                        "name" => "扫码推事件",
                        "key"  => "rselfmenu_0_1",
                    ],
                ],
            ],
            [
                "name"       => "菜单",
                "sub_button" => [
                    [
                        "type" => "view",
                        "name" => "搜索",
                        "url"  => "http://www.soso.com/",
                    ],
                    [
                        "type" => "view",
                        "name" => "视频",
                        "url"  => "http://v.qq.com/",
                    ],
                    [
                        "type" => "click",
                        "name" => "赞一下我们",
                        "key"  => "V1001_GOOD",
                    ],
                ],
            ],
        ];
        $ret = $this->app->menu->add($buttons);
        dd($ret);
    }

    public function profile(Request $request)
    {
        if (empty($_SESSION['wechat_user'])) {
            $oauth                  = $this->app->oauth;
            $_SESSION['target_url'] = 'user/profile';
            return $oauth->setRequest($request)->redirect();
        }

        dd($_SESSION['wechat_user']);
    }

    public function oauth_callback(Request $request)
    {
        $oauth = $this->app->oauth;
        $user  = $oauth->setRequest($request)->user();
        $data  = $user->toArray();
        Log::info(var_export($data, true));
        $_SESSION['wechat_user'] = $data;
        $targetUrl               = empty($_SESSION['target_url']) ? '/' : $_SESSION['target_url'];
        header('location:' . $targetUrl);
    }
}
