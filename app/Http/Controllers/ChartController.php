<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Capitalization;
use App\Models\Stock;
use App\Models\StockAdi;
use App\Models\StockIndex;

class ChartController extends Controller
{
    private function _rate($v1, $v2)
    {
        if ($v1 > 0) {
            $r = round(($v2 - $v1) * 100 / $v1, 2);
            if ($r > 0) {
                $r = "+$r";
            }
            $r = " ($r%)";
        } else {
            $r = '';
        }

        return $r;
    }

    public function getIndex(Request $request)
    {
        $start = $request->input('start', date('Y-m-d', strtotime('-3 month')));
        $end   = $request->input('end', date('Y-m-d'));

        $data = StockIndex::whereBetween('date', [$start, $end])->orderBy('date', 'asc')->get();

        $label = $price = $volume = [];
        $last = 0;
        foreach ($data as $row) {
            $r    = $this->_rate($last, $row['price']);
            $flag = ($row['price'] - $last < 0) ? 1 : 0;
            $last = $row['price'];

            $label[]  = $row['date'];
            $price[]  = ['value' => $row['price'], 'extra' => $r];
            $volume[] = ['value' => $row['volume'], 'extra' => $flag];
        }

        return view('charts.index', [
            'callback' => $request->input('callback'),
            'start'    => $start,
            'end'      => $end,
            'label'    => json_encode($label),
            'price'    => json_encode($price),
            'volume'   => json_encode($volume)
        ]);
    }

    public function getADI(Request $request)
    {
        $start = $request->input('start', date('Y-m-d', strtotime('-3 month')));
        $end   = $request->input('end', date('Y-m-d'));

        $data = StockAdi::whereBetween('date', [$start, $end])->orderBy('date', 'asc')->get();

        $label = $value = [];
        foreach ($data as $row) {
            $label[] = $row['date'];
            $value[] = $row['value'];
        }

        return view('charts.adi', [
            'callback' => $request->input('callback'),
            'start'    => $start,
            'end'      => $end,
            'label'    => json_encode($label),
            'value'    => json_encode($value)
        ]);
    }

    public function getPe($code, Request $request)
    {
        $start = $request->input('start', date('Y-m-d', strtotime('-3 month')));
        $end   = $request->input('end', date('Y-m-d'));

        $stock = Stock::findOrFail($code);
        $data  = $stock->history()->whereBetween('date', [$start, $end])->orderBy('date')->get();

        $label = $pe = $price = [];
        foreach ($data as $row) {
            $label[] = $row['date'];
            $pe[]    = max($row['pe'], 0);
            $price[] = $row['close'];
        }

        $title = "{$stock->name}（{$code}）的市盈率变化曲线";

        return view('charts.pe', [
            'callback' => $request->input('callback'),
            'title'    => $title,
            'start'    => $start,
            'end'      => $end,
            'label'    => json_encode($label),
            'pe'       => json_encode($pe),
            'price'    => json_encode($price),
        ]);
    }

    public function getCapital(Request $request)
    {
        $start = $request->input('start', date('Y-m-d', strtotime('-3 month')));
        $end   = $request->input('end', date('Y-m-d'));
        $area  = $request->input('area', 'sh');

        $name  = ($area == 'sh') ? '上海' : '深圳';
        $title = "$name 市值变化曲线";

        $data = Capitalization::where('area', '=', $area)->whereBetween('date', [$start, $end])->orderBy('date', 'asc')->get();

        $label = $total = $market = $pe = [];
        $lastTotal = $lastMarket = $lastPE = 0;
        foreach ($data as $row) {
            $label[]  = $row['date'];
            $total[]  = ['value' => $row['total'], 'extra' => $this->_rate($lastTotal, $row['total'])];
            $market[] = ['value' => $row['market'], 'extra' => $this->_rate($lastMarket, $row['market'])];
            $pe[]     = ['value' => $row['pe'], 'extra' => $this->_rate($lastPE, $row['pe'])];

            $lastTotal  = $row['total'];
            $lastMarket = $row['market'];
            $lastPE     = $row['pe'];
        }

        return view('charts.capital', [
            'callback' => $request->input('callback'),
            'title'    => $title,
            'start'    => $start,
            'end'      => $end,
            'label'    => json_encode($label),
            'total'    => json_encode($total),
            'market'   => json_encode($market),
            'pe'       => json_encode($pe),
        ]);
    }

    public function getTurnover(Request $request)
    {
        $start = $request->input('start', date('Y-m-d', strtotime('-3 month')));
        $end   = $request->input('end', date('Y-m-d'));

        $title = "成交额变化曲线";

        $data = Capitalization::whereBetween('date', [$start, $end])->orderBy('date', 'asc')->get();

        $stat = [];
        foreach ($data as $row) {
            $stat[$row['date']][$row['area']] = $row['turnover'];
        }

        $label = $total = $sz = $sh = [];
        foreach ($stat as $date => $row) {
            $label[] = $date;
            $sh[]    = $row['sh'];
            $sz[]    = $row['sz'];
            $total[] = $row['sh'] + $row['sz'];
        }

        return view('charts.turnover', [
            'callback' => $request->input('callback'),
            'title'    => $title,
            'start'    => $start,
            'end'      => $end,
            'label'    => json_encode($label),
            'total'    => json_encode($total),
            'sh'       => json_encode($sh),
            'sz'       => json_encode($sz),
        ]);
    }

    public function getEx(Request $request)
    {
        $start = $request->input('start', date('Y-m-d', strtotime('-3 month')));
        $end   = $request->input('end', date('Y-m-d'));

        $title = "换手率变化曲线";

        $data = Capitalization::whereBetween('date', [$start, $end])->orderBy('date', 'asc')->get();

        $stat = $data2 = [];
        foreach ($data as $row) {
            $stat[$row['date']][$row['area']] = $row['ex'];
            $data2[$row['date']][$row['area']] = $row;
        }

        $label = $total = $sz = $sh = [];
        foreach ($stat as $date => $row) {
            $label[] = $date;
            $sh[]    = $row['sh'];
            $sz[]    = $row['sz'];
            $total[] = round(($data2[$date]['sh']['turnover'] + $data2[$date]['sz']['turnover']) * 100 / ($data2[$date]['sh']['market'] + $data2[$date]['sz']['market']), 2);
        }

        return view('charts.ex', [
            'callback' => $request->input('callback'),
            'title'    => $title,
            'start'    => $start,
            'end'      => $end,
            'label'    => json_encode($label),
            'total'    => json_encode($total),
            'sh'       => json_encode($sh),
            'sz'       => json_encode($sz),
        ]);
    }
}
