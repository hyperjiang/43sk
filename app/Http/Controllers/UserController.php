<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getInfo(Request $request, $id)
    {
        $user = User::findOrFail($id);

        return view('user.info', [
            'user' => $user,
        ]);
    }

    public function postInfo(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $data['name']  = $request->input('name');
        $rules['name'] = 'required|max:255';

        if (!empty($request->input('password'))) {
            $data['password'] = $request->input('password');
            $data['password_confirmation'] = $request->input('password_confirmation');
            $rules['password'] = 'required|confirmed|min:6';
        }

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user->update([
            'name'     => $data['name'],
            'password' => bcrypt($data['password']),
        ]);

        return back();
    }
}
