<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Gate;
use Illuminate\Http\Request;
use App\Models\Capitalization;
use App\Models\MonthStat;
use App\Models\Stock;
use App\Models\StockCustomize;
use App\Models\StockHistory;
use App\Models\StockIndex;
use App\Models\StockRate;
use App\Models\StockStat;
use App\Models\StockStatAll;
use App\Models\StockStatHistory;

class StockController extends Controller
{
    public function getList(Request $request)
    {
        $keyword = $request->input('keyword');
        $sort    = $request->input('sort', 'rate');
        $direct  = $request->input('direct', 'desc');
        $chance  = $request->input('chance', 0);
        $own     = $request->input('own', 0);

        $selects = ['stock.*', 'stock_stat.*'];

        if ($sort != 'code') {
            $query = Stock::orderBy($sort, $direct)->orderBy('stock.code', 'asc');
        } else {
            $query = Stock::orderBy('stock.code', $direct);
        }

        $query->join('stock_stat', 'stock.code', '=', 'stock_stat.code');

        $keyword = trim($keyword);
        if (!empty($keyword)) {
            $query->where(function ($query) use ($keyword) {
                $query->where('stock.code', 'like', "%$keyword%")
                    ->orwhere('stock.name', 'like', "%$keyword%");
            });
        }

        if ($chance > 0) {
            $query->leftJoin('stock_stat_single as s1', function ($join) {
                $join->on('stock.code', '=', 's1.code')
                    ->on('stock_stat.trend', '=', 's1.days')
                    ->on('s1.type', '=', DB::raw('"trend"'));
            })
            ->where('s1.rate', '>=', $chance);

            $query->leftJoin('stock_stat_single as s2', function ($join) {
                $join->on('stock.code', '=', 's2.code')
                    ->on('stock_stat.trend', '=', 's2.days')
                    ->on('s2.type', '=', DB::raw('"line"'));
            })
            ->where('s2.rate', '>=', $chance);

            $selects = array_merge($selects, ['s1.rate as rate1', 's2.rate as rate2']);
        }

        if ($own && Auth::check()) {
            $query->join('stock_customize as c', 'c.code', '=', 'stock.code')
                ->where('c.uid', '=', Auth::user()->id);
            $selects = array_merge($selects, [
                'c.add_value',
                'c.date',
                DB::raw('round((total_value - add_value) * 100 / add_value, 2) as addup')
            ]);
        }

        $query->select($selects);

        $stocks = $query->paginate(50);

        return view('list', [
            'stocks'  => $stocks,
            'keyword' => $keyword,
            'chance'  => $chance,
            'sort'    => $sort,
            'direct'  => $direct,
            'own'     => $own
        ]);
    }

    public function getStat($code)
    {
        $stock = Stock::findOrFail($code);

        $allTrend = StockStatAll::where('type', '=', 'trend')->orderBy('days')->get();
        $allLine  = StockStatAll::where('type', '=', 'line')->orderBy('days')->get();

        $trend = $stock->allStat()->where('type', '=', 'trend')->orderBy('days')->get();
        $line  = $stock->allStat()->where('type', '=', 'line')->orderBy('days')->get();

        for ($i = -10; $i <= 10; $i++) {
            if ($i == -10) {
                $sel = Stock::whereRaw('round(rate, 1) <= -10');
            } elseif ($i == 10) {
                $sel = Stock::whereRaw('round(rate, 1) >= 10');
            } elseif ($i < 0) {
                $sel = Stock::whereRaw('round(rate, 1) > ?', [$i - 1])
                    ->whereRaw('round(rate, 1) <= ?', [$i]);
            } elseif ($i > 0) {
                $sel = Stock::whereRaw('round(rate, 1) >= ?', [$i])
                    ->whereRaw('round(rate, 1) < ?', [$i + 1]);
            } elseif ($i == 0) {
                Stock::whereRaw('round(rate, 1) > -1 and round(rate, 1) < 1');
            }
            $stat[$i] = $sel->count();
        }

        $rise = $fall = $r = $f = $u = $l = $c = $c1 = $c2 = $last = $d = $g = $p = 0;
        $history = $stock->history()->orderBy('date')->get();
        foreach ($history as $h) {
            if ($h->open == 0) continue;
            $upper = round(($h->high - $h->open) * 100 / $h->open, 2); // 上影
            $lower = round(($h->low - $h->open) * 100 / $h->open, 2); // 下影
            $u += $upper;
            $l += $lower;
            $c++;
            if ($upper != 0) $c1++;
            if ($lower != 0) $c2++;

            if ($h->rate > 0) { // 上涨
                $r++;
                $rise += $h->rate;
            } elseif ($h->rate < 0) { // 下跌
                $f++;
                $fall += $h->rate;
            }

            if ($last > 0) {
                if ($last > $h->open) { // 低开
                    $d++;
                } elseif ($last < $h->open) { // 高开
                    $g++;
                } elseif ($last == $h->open) { // 平开
                    $p++;
                }
            }
            $last = $h->close;
        }
        $avgUpper  = round($u / $c, 2);
        $avgLower  = round($l / $c, 2);
        $upperRate = round($c1 * 100 / $c, 2);
        $lowerRate = round($c2 * 100 / $c, 2);
        $avgRise   = round($rise / $c, 2);
        $avgFall   = round($fall / $c, 2);
        $riseRate  = round($r * 100 / $c, 2);
        $fallRate  = round($f * 100 / $c, 2);

        $gRate = round($g * 100 / $c, 2);
        $dRate = round($d * 100 / $c, 2);
        $pRate = round($p * 100 / $c, 2);

        $title = "{$stock->name}（{$code}）的统计情况";

        return view('stat', [
            'stock'     => $stock,
            'trend'     => $trend,
            'line'      => $line,
            'allTrend'  => $allTrend,
            'allLine'   => $allLine,
            'stat'      => $stat,
            'pageTitle' => $title,
            'metaDesc'  => $title,
            'avgUpper'  => $avgUpper,
            'avgLower'  => $avgLower,
            'upperRate' => $upperRate,
            'lowerRate' => $lowerRate,
            'avgRise'   => $avgRise,
            'avgFall'   => $avgFall,
            'riseRate'  => $riseRate,
            'fallRate'  => $fallRate,
            'gRate'     => $gRate,
            'dRate'     => $dRate,
            'pRate'     => $pRate,
            'total'     => Stock::count(),
        ]);
    }

    public function getHistory($code)
    {
        $stock = Stock::findOrFail($code);

        $title = "{$stock->name}（{$code}）的历史价格";

        $query = $stock->history()->orderBy('date', 'desc');

        $data = $query->paginate(50);

        return view('history', [
            'pageTitle' => $title,
            'metaDesc'  => $title,
            'data'      => $data,
            'stock'     => $stock,
        ]);
    }

    public function getMonthStat(Request $request)
    {
        $type = $request->input('type', 'sh');
        switch ($type) {
            case 'sh':
                $title = '上证综指';
                break;
            case 'sz':
                $title = '深证成指';
                break;
            case 'hs':
                $title = '沪深300';
                break;
            case 'zx':
                $title = '中小板指';
                break;
            case 'cy':
                $title = '创业板指';
                break;
        }
        $pageTitle = "$title 月份统计";

        $data = $stat = $ratio = [];
        $rows = MonthStat::where('type', '=', $type)
                    ->orderBy('year')
                    ->orderBy('month')
                    ->get();

        foreach ($rows as $row) {
            $data[$row->year][$row->month] = $row->rate;
            empty($stat[$row->month]) && $stat[$row->month] = 0;
            $stat[$row->month] += $row->rate;
            empty($ratio[$row->month]) && $ratio[$row->month] = 0;
            if ($row->rate > 0) $ratio[$row->month]++;
            empty($count[$row->month]) && $count[$row->month] = 0;
            $count[$row->month]++;
        }

        foreach ($ratio as $k => $v) {
            $ratio[$k] = round($v * 100/ $count[$k], 2);
        }

        return view('month-stat', [
            'pageTitle' => $pageTitle,
            'metaDesc'  => $pageTitle,
            'type'      => $type,
            'data'      => $data,
            'stat'      => $stat,
            'ratio'     => $ratio
        ]);
    }

    public function getIndex(Request $request)
    {
        $start = $request->input('start', date('Y-m-d', strtotime('-3 month')));
        $end   = $request->input('end', date('Y-m-d'));

        $minPrice  = StockIndex::where('price', '=', StockIndex::min('price'))->orderBy('date', 'desc')->firstOrFail()->toArray();
        $minVolume = StockIndex::where('volume', '=', StockIndex::min('volume'))->orderBy('date', 'desc')->firstOrFail()->toArray();
        $maxPrice  = StockIndex::where('price', '=', StockIndex::max('price'))->orderBy('date', 'desc')->firstOrFail()->toArray();
        $maxVolume = StockIndex::where('volume', '=', StockIndex::max('volume'))->orderBy('date', 'desc')->firstOrFail()->toArray();
        $current   = StockIndex::orderBy('date', 'desc')->firstOrFail()->toArray();

        return view('index', [
            'pageTitle' => '死多指数',
            'metaDesc'  => '死多指数是指所有A股的平均价和平均成交量',
            'start'     => $start,
            'end'       => $end,
            'minPrice'  => $minPrice,
            'minVolume' => $minVolume,
            'maxPrice'  => $maxPrice,
            'maxVolume' => $maxVolume,
            'current'   => $current,
        ]);
    }

    public function getPe($code, Request $request)
    {
        $start = $request->input('start', date('Y-m-d', strtotime('-3 month')));
        $end   = $request->input('end', date('Y-m-d'));

        $stock = Stock::findOrFail($code);

        $title = "{$stock->name}（{$code}）的市盈率变化曲线";

        return view('pe', [
            'pageTitle' => $title,
            'metaDesc'  => $title,
            'stock'     => $stock,
            'start'     => $start,
            'end'       => $end,
        ]);
    }

    public function getCapital(Request $request)
    {
        $start = $request->input('start', date('Y-m-d', strtotime('-3 month')));
        $end   = $request->input('end', date('Y-m-d'));

        $minData = $maxData = [];

        $columns = [
            'total'    => '市值（亿）',
            'market'   => '流通市值（亿）',
            'turnover' => '成交额（亿）',
            'pe'       => '市盈率',
            'ex'       => '换手率'
        ];

        $colors = [
            'total'    => 'active',
            'market'   => 'success',
            'turnover' => 'info',
            'pe'       => 'warning',
            'ex'       => 'danger'
        ];

        $areas = ['sz', 'sh'];
        foreach ($columns as $column => $desc) {
            foreach ($areas as $area) {
                $minData[$area][$column] = Capitalization::select('date', "$column as value")
                    ->where('area', '=', $area)
                    ->where($column, '=', Capitalization::where('area', '=', $area)->min($column))
                    ->firstOrFail()
                    ->toArray();

                $maxData[$area][$column] = Capitalization::select('date', "$column as value")
                    ->where('area', '=', $area)
                    ->where($column, '=', Capitalization::where('area', '=', $area)->max($column))
                    ->firstOrFail()
                    ->toArray();
            }
        }

        $data = Capitalization::select(
                    'date',
                    DB::raw('sum(total) as total'),
                    DB::raw('sum(market) as market'),
                    DB::raw('sum(turnover) as turnover'),
                    DB::raw('sum(pe)/2 as pe'),
                    DB::raw('sum(turnover) * 100 / sum(market) as ex')
                )
                ->groupBy('date')
                ->get();

        foreach ($data as $row) {
            foreach ($columns as $column => $desc) {
                if (empty($minData['all'][$column]['value']) || $minData['all'][$column]['value'] > $row[$column]) {
                    $minData['all'][$column] = ['date' => $row['date'], 'value' => $row[$column]];
                }

                if (empty($maxData['all'][$column]['value']) || $maxData['all'][$column]['value'] < $row[$column]) {
                    $maxData['all'][$column] = ['date' => $row['date'], 'value' => $row[$column]];
                }
            }
        }

        foreach ($areas as $area) {
            $current[$area] = Capitalization::where('area', '=', $area)->orderBy('date', 'desc')->firstOrFail()->toArray();
        }
        foreach ($columns as $column => $desc) {
            $current['all'][$column] = $current['sh'][$column] + $current['sz'][$column];
        }
        $current['all']['date'] = $current['sh']['date'];
        $current['all']['pe'] /= 2;

        // 格式化数据
        foreach (['sh', 'sz', 'all'] as $area) {
            foreach ($columns as $column => $desc) {
                if ($column == 'pe') {
                    $minData[$area][$column]['value'] = round($minData[$area][$column]['value'], 2);
                    $maxData[$area][$column]['value'] = round($maxData[$area][$column]['value'], 2);
                    $current[$area][$column] = round($current[$area][$column], 2);
                } elseif ($column == 'ex') {
                    $minData[$area][$column]['value'] = round($minData[$area][$column]['value'], 2) . '%';
                    $maxData[$area][$column]['value'] = round($maxData[$area][$column]['value'], 2) . '%';
                    $current[$area][$column] = round($current[$area][$column], 2) . '%';
                } else {
                    $minData[$area][$column]['value'] = number_format($minData[$area][$column]['value']);
                    $maxData[$area][$column]['value'] = number_format($maxData[$area][$column]['value']);
                    $current[$area][$column] = number_format($current[$area][$column]);
                }
            }
        }

        $title = '两市市值';

        return view('capital', [
            'pageTitle' => $title,
            'metaDesc'  => $title,
            'start'     => $start,
            'end'       => $end,
            'columns'   => $columns,
            'colors'    => $colors,
            'minData'   => $minData,
            'maxData'   => $maxData,
            'current'   => $current,
        ]);
    }

    public function getRank(Request $request)
    {
        $type    = $request->input('type', 'week');
        $keyword = $request->input('keyword');
        $sort    = $request->input('sort', 'rate');
        $direct  = $request->input('direct', 'desc');

        $title = "涨跌榜";

        $query = StockRate::from('stock_rate as r')
            ->select('r.*', 'stock.name', 'stock.pe', 'st.trend', 'st.line')
            ->join('stock', 'r.code', '=', 'stock.code')
            ->join('stock_stat as st', 'r.code', '=', 'st.code')
            ->where('r.type', '=', $type);

        // 根据关键字搜索
        if (!empty($keyword)) {
            $query->where(function ($query) use ($keyword) {
                $query->where('stock.code', 'like', "%$keyword%")
                    ->orwhere('stock.name', 'like', "%$keyword%");
            });
        }

        // 排序
        $query->orderBy($sort, $direct);

        $data = $query->paginate(50);

        return view('rank', [
            'pageTitle' => $title,
            'metaDesc'  => $title,
            'data'      => $data,
            'type'      => $type,
            'keyword'   => $keyword,
            'sort'      => $sort,
            'direct'    => $direct,
        ]);
    }

    public function getHot(Request $request)
    {
        $rate  = $request->input('rate', 50);
        $start = $request->input('start', date('Y-m-d', strtotime('-5 days')));
        $end   = $request->input('end', date('Y-m-d'));

        $title = "热股";

        $data = StockHistory::select(
                    'code', 'name',
                    DB::raw('sum(ex) as ex'),
                    DB::raw('sum(amount) as amount'),
                    DB::raw('sum(volume) as volume'),
                    DB::raw('sum(amount) / sum(volume) as avg')
                )
                ->whereBetween('date', [$start, $end])
                ->groupBy('code')
                ->get();

        $stat = [];
        foreach ($data as $row) {
            if ($row['ex'] < $rate) continue;
            $row['open'] = StockHistory::where('code', '=', $row['code'])
                            ->where('date', '>=', $start)
                            ->orderBy('date')
                            ->value('open');
            $row['close'] = StockHistory::where('code', '=', $row['code'])
                            ->where('date', '<=', $end)
                            ->orderBy('date', 'desc')
                            ->value('close');
            $row['rate'] = round(($row['close'] - $row['open']) * 100 / $row['open'], 2);
            $stat[] = $row;
        }

        usort($stat, function ($a, $b) {
            if ($a['ex'] == $b['ex']) return 0;
            return ($a['ex'] > $b['ex']) ? -1 : 1;
        });

        return view('hot', [
            'pageTitle' => $title,
            'metaDesc'  => $title,
            'data'      => $stat,
            'rate'      => $rate,
            'start'     => $start,
            'end'       => $end,
        ]);
    }

    public function postUpload(Request $request)
    {
        if (Gate::denies('upload-stock')) {
            abort(403);
        }

        $arr = explode('.', $_FILES['ag']['name']);
        $date = $arr[0];
        $date = date('Y-m-d', strtotime($date));

        $data = file($_FILES['ag']['tmp_name']);

        $keys = null;

        foreach ($data as $row) {

            $row = explode("\t", $row);

            if ($keys === null) {
                $row[0] = '代码';
                $keys = $row;
                continue;
            }
            $info = array_combine($keys, $row);

            if (empty($info['总金额']) || !is_numeric($info['总金额'])) continue;

            if (strpos($info['总量'], '万') !== false) {
                $info['总量'] = str_replace('万', '', $info['总量']) * 10000;
            }

            $stock = array(
                'code' => $info['代码'],
                'name' => $info['名称'],
                'rate' => $info['涨幅%%'],
                'close' => $info['现价'],
                'open' => $info['今开'],
                'low' => $info['最低'],
                'high' => $info['最高'],
                'volume' => $info['总量'] * 100,
                'amount' => $info['总金额'],
                'pe' => $info['市盈(动)'],
                'ex' => $info['换手%%'],
                'last' => $info['昨收'],
                'avg' => $info['均价'],
                'market_value' => str_replace('亿', '', $info['流通市值']),
                'total_value' => str_replace('亿', '', $info['AB股总市值']),
            );

            if ($stock['code'][0] == 6) {
                $stock['area'] = 'sh';
            } else {
                $stock['area'] = 'sz';
            }

            $st = Stock::find($stock['code']);
            if ($st) {
                $st->update($stock);
            } else {
                Stock::create($stock);
            }

            unset($stock['area']);
            unset($stock['last']);
            $stock['date'] = $date;

            $history = StockHistory::where('code', '=', $stock['code'])
                ->where('date', '=', $stock['date'])
                ->first();
            if ($history) {
                $history->update($stock);
            } else {
                StockHistory::create($stock);
            }

            $stat = StockStatHistory::where('code', '=', $stock['code'])
                    ->where('date', '<', $stock['date'])
                    ->orderBy('date', 'desc')
                    ->first();
            if (!empty($stat)) {
                $stat = $stat->toArray();
            } else {
                $stat = ['trend' => 0, 'line' => 0];
            }
            if ($stock['rate'] > 0) {
                if ($stat['trend'] > 0) {
                    $stat['trend']++;
                } else {
                    $stat['trend'] = 1;
                }
            } elseif ($stock['rate'] < 0) {
                if ($stat['trend'] < 0) {
                    $stat['trend']--;
                } else {
                    $stat['trend'] = -1;
                }
            } else {
                $stat['trend'] = 0;
            }

            if ($stock['close'] > $stock['open']) {
                if ($stat['line'] > 0) {
                    $stat['line']++;
                } else {
                    $stat['line'] = 1;
                }
            } elseif ($stock['close'] < $stock['open']) {
                if ($stat['line'] < 0) {
                    $stat['line']--;
                } else {
                    $stat['line'] = -1;
                }
            } else {
                $stat['line'] = 0;
            }
            $stat['code'] = $stock['code'];
            $stat['date'] = $stock['date'];

            unset($stat['id']);
            $statHistory = StockStatHistory::where('code', '=', $stock['code'])
                ->where('date', '=', $stock['date'])
                ->first();
            if ($statHistory) {
                $statHistory->update($stat);
            } else {
                StockStatHistory::create($stat);
            }

            unset($stat['date']);
            $stockStat = StockStat::find($stock['code']);
            if ($stockStat) {
                $stockStat->update($stat);
            } else {
                StockStat::create($stat);
            }
        }

        return redirect('index');
    }

    public function add($code)
    {
        $stock = Stock::findOrFail($code);

        $data = [
            'uid'       => Auth::user()->id,
            'code'      => $code,
            'date'      => date('Y-m-d'),
            'price'     => $stock->close,
            'add_value' => $stock->total_value
        ];

        $sc = StockCustomize::where('uid', '=', $data['uid'])
            ->where('code', '=', $code)
            ->first();
        if (!$sc) {
            DB::table('stock_customize')->insert($data);
        }

        return redirect(route('stock.list', ['own' => 1]));
    }

    public function delete($code)
    {
        StockCustomize::where('uid', '=', Auth::user()->id)
            ->where('code', '=', $code)
            ->delete();

        return redirect(route('stock.list', ['own' => 1]));
    }
}
