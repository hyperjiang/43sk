<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

// Authentication and Registration routes
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');
Route::post('register', 'Auth\AuthController@postRegister');

// Password reset link request routes
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// User routes
Route::get('user/info/{id}', 'UserController@getInfo');
Route::post('user/info/{id}', 'UserController@postInfo');

// Application routes
Route::get('/', [
    'as' => 'stock.list', 'uses' => 'StockController@getList',
]);

Route::get('stock/{code}', [
    'as' => 'stock.stat', 'uses' => 'StockController@getStat',
]);

Route::get('history/{code}', [
    'as' => 'stock.history', 'uses' => 'StockController@getHistory',
]);

Route::get('pe/{code}', [
    'as' => 'stock.pe', 'uses' => 'StockController@getPe',
]);

Route::get('month-stat', [
    'as' => 'month.stat', 'uses' => 'StockController@getMonthStat',
]);

Route::get('rank', [
    'as' => 'stock.rank', 'uses' => 'StockController@getRank',
]);

Route::get('hot', [
    'as' => 'stock.hot', 'uses' => 'StockController@getHot',
]);

Route::get('upload', [
    'as' => 'stock.upload', 'middleware' => 'auth', function () {
        return view('upload');
    },
]);

Route::post('upload', [
    'as' => 'stock.upload', 'uses' => 'StockController@postUpload', 'middleware' => 'auth',
]);

Route::get('stock/add/{code}', [
    'as' => 'stock.add', 'uses' => 'StockController@add', 'middleware' => 'auth',
]);

Route::get('stock/delete/{code}', [
    'as' => 'stock.add', 'uses' => 'StockController@delete', 'middleware' => 'auth',
]);

Route::get('about', function () {
    return view('about');
});

Route::get('index', [
    'as' => 'stock.index', 'uses' => 'StockController@getIndex',
]);

Route::get('capital', [
    'as' => 'stock.capital', 'uses' => 'StockController@getCapital',
]);

Route::get('chart-index', [
    'as' => 'chart.index', 'uses' => 'ChartController@getIndex',
]);

Route::get('chart-adi', [
    'as' => 'chart.adi', 'uses' => 'ChartController@getADI',
]);

Route::get('chart-capital', [
    'as' => 'chart.capital', 'uses' => 'ChartController@getCapital',
]);

Route::get('chart-turnover', [
    'as' => 'chart.turnover', 'uses' => 'ChartController@getTurnover',
]);

Route::get('chart-ex', [
    'as' => 'chart.ex', 'uses' => 'ChartController@getEx',
]);

Route::get('chart-pe/{code}', [
    'as' => 'chart.pe', 'uses' => 'ChartController@getPe',
]);

/* below just for test */

// Simple Markdown Editor
Route::get('md', function () {
    return view('md');
});

Route::any('wx', [
    'as' => 'wx', 'uses' => 'WxController@handle',
]);

Route::get('wx/menu', ['uses' => 'WxController@getMenu']);

Route::get('wx/menu/create', ['uses' => 'WxController@createMenu']);

Route::get('wx/profile', ['uses' => 'WxController@profile']);

Route::get('wx/oauth_callback', ['uses' => 'WxController@oauth_callback']);

Route::get('wx/send-msg', ['uses' => 'WxController@sendMsg']);
