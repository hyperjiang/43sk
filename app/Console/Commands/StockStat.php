<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Stock;
use App\Models\StockHistory;
use App\Models\StockStatHistory;

class StockStat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:stat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run stock statistics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->updateIndex();
        $this->updateAdi();
        $this->updateLaw();
        $this->updateRate();

        $this->info('更新完成');
    }

    public function updateIndex()
    {
        $this->info('更新死多指数');

        $data = StockHistory::select(
                DB::raw('avg(close) as price'),
                DB::raw('avg(volume) as volume'),
                'date'
            )
            ->groupBy('date')
            ->orderBy('date', 'desc')
            ->first();

        $data['price'] = round($data['price'] * 100);
        $data['volume'] = round($data['volume'] / 100);

        DB::insert(
            'replace into stock_index (price, volume, date) values (?, ?, ?)',
            [$data['price'], $data['volume'], $data['date']]
        );
    }

    public function updateAdi()
    {
        $this->info('更新ADI指数');

        $date = StockHistory::max('date');
        $data = StockHistory::where('date', '=', $date)->get();

        $s1 = $s2 = 0;
        foreach ($data as $row) {
            if ($row['rate'] > 0) $s1 += $row['amount'];
            else if ($row['rate'] < 0) $s2 += $row['amount'];
        }
        $s = round(($s1 - $s2) / 10000);
        DB::insert('replace into stock_adi (date, value) values (?, ?)', [$date, $s]);
    }

    public function updateLaw()
    {
        $this->info('更新股票规律统计');

        $stocks = Stock::get();

        $trend1 = $trend2 = $line1 = $line2 = []; // 总统计

        $startDate = date('Y-01-01');

        foreach ($stocks as $stock) {

            $code = $stock['code'];
            $singleTrend1 = $singleTrend2 = $singleLine1 = $singleLine2 = []; // 具体股票统计

            $data = StockStatHistory::where('code', '=', $code)
                        ->where('date', '>=', $startDate)
                        ->orderBy('date')
                        ->get();

            $lastTrend = $lastLine = null;
            foreach ($data as $row) {
                if (isset($trend1[$row['trend']])) {
                    $trend1[$row['trend']]++;
                } else {
                    $trend1[$row['trend']] = 1;
                }
                if (isset($line1[$row['line']])) {
                    $line1[$row['line']]++;
                } else {
                    $line1[$row['line']] = 1;
                }
                if (isset($singleTrend1[$row['trend']])) {
                    $singleTrend1[$row['trend']]++;
                } else {
                    $singleTrend1[$row['trend']] = 1;
                }
                if (isset($singleLine1[$row['line']])) {
                    $singleLine1[$row['line']]++;
                } else {
                    $singleLine1[$row['line']] = 1;
                }

                if ($lastTrend !== null && $lastLine !== null) {
                    if ($row['trend'] > 0 && $row['trend'] > $lastTrend) {
                        if (isset($trend2[$lastTrend])) {
                            $trend2[$lastTrend]++;
                        } else {
                            $trend2[$lastTrend] = 1;
                        }

                        if (isset($singleTrend2[$lastTrend])) {
                            $singleTrend2[$lastTrend]++;
                        } else {
                            $singleTrend2[$lastTrend] = 1;
                        }
                    }

                    if ($row['line'] > 0 && $row['line'] > $lastLine) {
                        if (isset($line2[$lastLine])) {
                            $line2[$lastLine]++;
                        } else {
                            $line2[$lastLine] = 1;
                        }

                        if (isset($singleLine2[$lastLine])) {
                            $singleLine2[$lastLine]++;
                        } else {
                            $singleLine2[$lastLine] = 1;
                        }
                    }
                }

                $lastTrend = $row['trend'];
                $lastLine = $row['line'];
            } // foreach

            $singleStat1 = $singleStat2 = [];
            foreach ($singleTrend2 as $key => $val) {
                $singleStat1[$key] = number_format(($val * 100 / $singleTrend1[$key]), 2);
            }
            foreach ($singleLine2 as $key => $val) {
                $singleStat2[$key] = number_format(($val * 100 / $singleLine1[$key]), 2);
            }
            ksort($singleTrend1);
            ksort($singleLine1);

            foreach ($singleTrend1 as $key => $val) {
                $update = [
                    'code'  => $code,
                    'days'  => $key,
                    'type'  => 'trend',
                    'num'   => $val,
                    'rate'  => !empty($singleStat1[$key]) ? $singleStat1[$key] : 0
                ];
                DB::insert('replace into stock_stat_single (code, days, type, num, rate) values (?, ?, ?, ?, ?)', $update);
            }
            foreach ($singleLine1 as $key => $val) {
                $update = [
                    'code'  => $code,
                    'days'  => $key,
                    'type'  => 'line',
                    'num'   => $val,
                    'rate'  => !empty($singleStat2[$key]) ? $singleStat2[$key] : 0
                ];
                DB::insert('replace into stock_stat_single (code, days, type, num, rate) values (?, ?, ?, ?, ?)', $update);
            }
        }

        $stat1 = $stat2 = [];
        foreach ($trend2 as $key => $val) {
            $stat1[$key] = number_format(($val * 100 / $trend1[$key]), 2);
        }
        foreach ($line2 as $key => $val) {
            $stat2[$key] = number_format(($val * 100 / $line1[$key]), 2);
        }

        ksort($trend1);
        ksort($line1);
        ksort($stat1);
        ksort($stat2);

        foreach ($trend1 as $key => $val) {
            $update = [
                'days'  => $key,
                'type'  => 'trend',
                'num'   => $val,
                'rate'  => !empty($stat1[$key]) ? $stat1[$key] : 0
            ];
            DB::insert('replace into stock_stat_all (days, type, num, rate) values (?, ?, ?, ?)', $update);
        }
        foreach ($line1 as $key => $val) {
            $update = [
                'days'  => $key,
                'type'  => 'line',
                'num'   => $val,
                'rate'  => !empty($stat2[$key]) ? $stat2[$key] : 0
            ];
            DB::insert('replace into stock_stat_all (days, type, num, rate) values (?, ?, ?, ?)', $update);
        }
    }

    public function updateRate()
    {
        $this->info('涨跌幅统计');

        $stocks = Stock::get();

        $year  = date('Y-01-01');
        $month = date('Y-m-01');
        $week  = date('Y-m-d', time() - (date('N') - 1) * 86400);

        foreach ($stocks as $stock) {
            if (empty($stock['open'])) continue;

            $s1 = StockHistory::where('code', '=', $stock['code'])
                ->where('date', '>=', $year)
                ->where('open', '>', 0)
                ->orderBy('date')
                ->first();
            if (!empty($s1)) {
                $arr = [
                    'code'  => $stock['code'],
                    'type'  => 'year',
                    'open'  => $s1['open'],
                    'close' => $stock['close'],
                    'rate'  => round(($stock['close'] - $s1['open']) * 100 / $s1['open'], 2)
                ];
                DB::insert('replace into stock_rate (code, type, open, close, rate) values (?, ?, ?, ?, ?)', $arr);
            } else {
                DB::delete('delete from stock_rate where code = ? and type = "year"', [$stock['code']]);
            }

            $s2 = StockHistory::where('code', '=', $stock['code'])
                ->where('date', '>=', $month)
                ->where('open', '>', 0)
                ->orderBy('date')
                ->first();
            if (!empty($s2)) {
                $arr = [
                    'code'  => $stock['code'],
                    'type'  => 'month',
                    'open'  => $s2['open'],
                    'close' => $stock['close'],
                    'rate'  => round(($stock['close'] - $s2['open']) * 100 / $s2['open'], 2)
                ];
                DB::insert('replace into stock_rate (code, type, open, close, rate) values (?, ?, ?, ?, ?)', $arr);
            } else {
                DB::delete('delete from stock_rate where code = ? and type = "month"', [$stock['code']]);
            }

            $s3 = StockHistory::where('code', '=', $stock['code'])
                ->where('date', '>=', $week)
                ->where('open', '>', 0)
                ->orderBy('date')
                ->first();
            if (!empty($s3)) {
                $arr = [
                    'code'  => $stock['code'],
                    'type'  => 'week',
                    'open'  => $s3['open'],
                    'close' => $stock['close'],
                    'rate'  => round(($stock['close'] - $s3['open']) * 100 / $s3['open'], 2)
                ];
                DB::insert('replace into stock_rate (code, type, open, close, rate) values (?, ?, ?, ?, ?)', $arr);
            } else {
                DB::delete('delete from stock_rate where code = ? and type = "week"', [$stock['code']]);
            }
        }
    }
}
