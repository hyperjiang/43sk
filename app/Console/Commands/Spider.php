<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Spider extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spider {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run spider';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = $this->argument('date');

        $this->scanSZ($date);
        $this->scanSH($date);

        $this->info('更新完成');
    }

    public function scanSH($date = '')
    {
        $this->info('抓取上交所指数');

        if (empty($date)) {
            $date = date('Y-m-d');
        }

        $url = "http://query.sse.com.cn/marketdata/tradedata/queryTradingByProdTypeData.do?jsonCallBack=jsonpCallback12345&searchDate=$date&prodType=gp&_=" . time() . '000';

        $client = new \GuzzleHttp\Client([
            'headers' => [
                'Referer' => 'http://www.sse.com.cn/market/stockdata/overview/day/',
            ],
        ]);

        $res = $client->get($url);

        $data = $res->getBody();

        $json = preg_replace('/jsonpCallback12345\((.+)\)/', '$1', $data);

        $arr = json_decode($json, true);

        $result = $arr['result'][0];

        $total = $result['marketValue'];

        if (empty($total)) {
            return;
        }

        $market = $result['negotiableValue'];
        $turnover = $result['trdAmt'];
        $pe = $result['profitRate'];
        $ex = round($turnover * 100 / $market, 2);

        DB::insert('replace into capitalization (area, date, total, market, turnover, pe, ex)
                values (?, ?, ?, ?, ?, ?, ?)', ['sh', $date, $total, $market, $turnover, $pe, $ex]);
    }

    public function scanSZ($date = '')
    {
        $this->info('抓取深交所指数');

        if (empty($date)) {
            $date = date('Y-m-d');
        }

        $tmpfile = '/tmp/sz.xlsx';
        $url = "http://www.szse.cn/api/report/ShowReport?SHOWTYPE=xlsx&CATALOGID=1803&TABKEY=tab1&txtQueryDate=$date";
        file_put_contents($tmpfile, file_get_contents($url));

        $spreadsheet = IOFactory::load($tmpfile);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        if (!empty($sheetData[8])) {
            $total = round(str_replace(',', '', $sheetData[11]['B']) / 100000000, 2);
            $market = round(str_replace(',', '', $sheetData[12]['B']) / 100000000, 2);
            $turnover = round(str_replace(',', '', $sheetData[8]['B']) / 100000000, 2);
            $pe = round($sheetData[15]['B'], 2);
            $ex = round($sheetData[16]['B'], 2);

            echo $total . ' ' . $market . ' ' . $turnover . ' ' . $pe . ' ' . $ex . "\n";

            DB::insert('replace into capitalization (area, date, total, market, turnover, pe, ex)
                values (?, ?, ?, ?, ?, ?, ?)', ['sz', $date, $total, $market, $turnover, $pe, $ex]);
        }
    }
}
