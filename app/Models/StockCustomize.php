<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockCustomize extends Model
{
    protected $table = 'stock_customize';

    protected $primaryKey = ['uid', 'code'];

    protected $guarded = [];

    public $timestamps = false;
}
