<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockHistory extends Model
{
    protected $table = 'stock_history';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public $timestamps = false;
}
