<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockStatSingle extends Model
{
    protected $table = 'stock_stat_single';

    protected $primaryKey = ['code', 'days', 'type'];

    public $timestamps = false;
}