<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockStatHistory extends Model
{
    protected $table = 'stock_stat_history';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public $timestamps = false;
}
