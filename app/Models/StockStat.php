<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockStat extends Model
{
    protected $table = 'stock_stat';

    protected $primaryKey = 'code';

    protected $guarded = [];

    public $timestamps = false;
}
