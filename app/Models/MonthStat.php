<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MonthStat extends Model
{
    protected $table = 'month_stat';

    protected $primaryKey = ['year', 'month', 'type'];

    public $timestamps = false;
}