<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Capitalization extends Model
{
    protected $table = 'capitalization';

    protected $primaryKey = ['area', 'date'];

    public $timestamps = false;
}
