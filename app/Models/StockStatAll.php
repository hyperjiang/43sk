<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockStatAll extends Model
{
    protected $table = 'stock_stat_all';

    protected $primaryKey = 'code';

    public $timestamps = false;
}