<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockIndex extends Model
{
    protected $table = 'stock_index';

    protected $primaryKey = 'date';

    public $timestamps = false;
}
