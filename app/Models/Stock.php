<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stock';

    protected $primaryKey = 'code';

    protected $guarded = [];

    public $timestamps = false;

    public function stat()
    {
        return $this->hasOne('App\Models\StockStat', 'code', 'code');
    }

    public function history()
    {
        return $this->hasMany('App\Models\StockHistory', 'code', 'code');
    }

    public function rate()
    {
        return $this->hasMany('App\Models\StockRate', 'code', 'code');
    }

    public function allStat()
    {
        return $this->hasMany('App\Models\StockStatSingle', 'code', 'code');
    }
}
