<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockRate extends Model
{
    protected $table = 'stock_rate';

    protected $primaryKey = ['code', 'type'];

    public $timestamps = false;
}
