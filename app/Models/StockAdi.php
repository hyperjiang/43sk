<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockAdi extends Model
{
    protected $table = 'stock_adi';

    protected $primaryKey = 'date';

    public $timestamps = false;
}
