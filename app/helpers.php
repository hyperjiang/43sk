<?php

if (! function_exists('color')) {
    function color($v, $suffix = '')
    {
        $color = ($v > 0) ? 'c1' : 'c2';
        return "<span class=\"$color\">{$v}{$suffix}</span>";
    }
}

if (! function_exists('light_keyword')) {
    function light_keyword($string, $keyword)
    {
        if ($string && $keyword) {
            if (!is_array($keyword)) {
                $keyword = array($keyword);
            }
            $keyword = array_map('trim', $keyword);
            $pattern = array();
            foreach ($keyword as $word) {
                $word && $pattern[] = '(' . preg_quote($word) . ')';
            }
            $pattern && $string = preg_replace(
                '/(' . implode('|', $pattern) . ')/is',
                '<span class="highlight">\\1</span>',
                $string
            );
        }
        return $string;
    }
}

if (! function_exists('arrow')) {
    function arrow($col, $sort, $direct)
    {
        if ($sort == $col) {
            if ($direct == 'asc') return '<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>';
            elseif ($direct == 'desc') return '<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>';
        }
        return '';
    }
}

if (! function_exists('select')) {
    function select($k, $v)
    {
        return $k == $v ? 'selected' : '';
    }
}

if (! function_exists('require_js')) {
    function require_js()
    {
        $baseUrl = env('JS_DIR', '/js');
        $urlArgs = config('app.version');

        return <<<RJS
<script src="{$baseUrl}/require.js" data-main="{$baseUrl}/app"></script>
<script src="{$baseUrl}/config.js?{$urlArgs}"></script>
<script type="text/javascript">
require.config({
      baseUrl: '{$baseUrl}',
      urlArgs: '{$urlArgs}'
});
</script>
RJS;
    }
}

if (! function_exists('curl')) {
    function curl($url, $data = null, array $options = null)
    {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL            => $url,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_RETURNTRANSFER => 1,
        ]);

        if ($data) {
            if (is_array($data)) {
                $query = http_build_query($data);
            } else {
                $query = $data;
            }
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-type: application/x-www-form-urlencoded',
                'Content-Length: ' . strlen($query),
            ]);
        }

        if ($options) {
            curl_setopt_array($ch, $options);
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}
